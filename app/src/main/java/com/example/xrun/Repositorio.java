package com.example.xrun;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;

import java.util.ArrayList;

public class Repositorio {

    /**
     * Repositorio con los metodos que vamos a  utilizar para interactuar con la base de datos
     */

    public static boolean insertarCorredor(Corredor c, Context contexto) {
        //Abrimos la base de datos en modo escritura
        BaseDeDatosSQLiteHelper baseDatosHelper = new BaseDeDatosSQLiteHelper(contexto, Constantes.nombreDB, null, 1);

        SQLiteDatabase db = baseDatosHelper.getWritableDatabase();

        //Si hemos abierto correctamente la base de datos
        if (db != null) {

            try {
                ContentValues values = new ContentValues();
                values.put(Constantes.nombreCorredor, c.getNombre());
                values.put(Constantes.edadCorredor, c.getEdad());
                values.put(Constantes.emailCorredor, c.getEmail());
                values.put(Constantes.codigoQR, c.getCodigo());
                values.put(Constantes.llegada, "El corredor aun no ha llegado");

                //Insertar un registro

                db.insert(Constantes.tablaCorredores, null, values);
            } catch (SQLiteException e) {
                return false;
            }

            //Cerramos la base de datos
            db.close();
            return true;
        } else {
            return false;
        }
    }

    public static boolean modificarCorredor (Corredor c, Context contexto, int idRecuperado, String fechaLlegada) {
        BaseDeDatosSQLiteHelper baseDatosHelper = new BaseDeDatosSQLiteHelper(contexto, Constantes.nombreDB, null, 1);

        SQLiteDatabase db = baseDatosHelper.getWritableDatabase();

        if (db != null) {

            try {
                ContentValues values = new ContentValues();
                values.put(Constantes.nombreCorredor, c.getNombre());
                values.put(Constantes.edadCorredor, c.getEdad());
                values.put(Constantes.emailCorredor, c.getEmail());
                values.put(Constantes.codigoQR, c.getCodigo());
                if (fechaLlegada == null) {
                    values.put(Constantes.llegada, "El corredor aun no ha llegado");
                } else {
                    values.put(Constantes.llegada, fechaLlegada);
                }
                String[] args = new String[] {Integer.toString(idRecuperado)};
                //Modificar un registro
                db.update(Constantes.tablaCorredores, values, "id=?", args);
            } catch (SQLiteException e) {
                return false;
            }
            //Cerramos la base de datos
            db.close();
            return true;
        } else {
            return false;
        }
    }

    public static ArrayList<Corredor> consultarBD(Context contexto) {

        ArrayList<Corredor> itemsPregunta = new ArrayList<Corredor>();
        BaseDeDatosSQLiteHelper baseDatosHelper = new BaseDeDatosSQLiteHelper(contexto, Constantes.nombreDB, null, 1);

        SQLiteDatabase db = baseDatosHelper.getWritableDatabase();

        Cursor c = db.rawQuery(Constantes.consultarTabla, null);


        if (c != null) {
            if (c.moveToFirst()) {
                do {
                    //Asignamos el valor en nuestras variables para usarlos en lo que necesitemos
                    int idCursor = c.getInt(c.getColumnIndex(Constantes.idCorredor));
                    String nombreCursor = c.getString(c.getColumnIndex(Constantes.nombreCorredor));
                    String edadCursor = c.getString(c.getColumnIndex(Constantes.edadCorredor));
                    String emailCursor = c.getString(c.getColumnIndex(Constantes.emailCorredor));
                    String codigoCursor = c.getString(c.getColumnIndex(Constantes.codigoQR));
                    String llegadaCursor = c.getString(c.getColumnIndex(Constantes.llegada));

                    itemsPregunta.add(new Corredor(idCursor, nombreCursor, edadCursor, emailCursor, codigoCursor, llegadaCursor));

                } while (c.moveToNext());
            }
        }

        c.close();
        db.close();

        return itemsPregunta;

    }

    public static Corredor recuperarCorredor(int idRecuperado, Context contexto) {

        BaseDeDatosSQLiteHelper baseDatosHelper = new BaseDeDatosSQLiteHelper(contexto, Constantes.nombreDB, null, 1);

        SQLiteDatabase db = baseDatosHelper.getWritableDatabase();

        String[] args = new String[] {Integer.toString(idRecuperado)};
        Cursor c = db.rawQuery(Constantes.recuperarCorredorModificar, args);

        if (c != null) {
            if (c.moveToFirst()) {

                int idCursor = c.getInt(c.getColumnIndex(Constantes.idCorredor));
                String nombreCursor = c.getString(c.getColumnIndex(Constantes.nombreCorredor));
                String edadCursor = c.getString(c.getColumnIndex(Constantes.edadCorredor));
                String emailCursor = c.getString(c.getColumnIndex(Constantes.emailCorredor));
                String codigoCursor = c.getString(c.getColumnIndex(Constantes.codigoQR));
                String fechaCursor = c.getString(c.getColumnIndex(Constantes.llegada));

                Corredor corredor = new Corredor(idCursor, nombreCursor, edadCursor, emailCursor, codigoCursor, fechaCursor);

                return corredor;
            }
        }

        c.close();
        db.close();
        return null;
    }

    public static Corredor getCorredorByEmail(String emailCorredor, Context contexto) {

        BaseDeDatosSQLiteHelper baseDatosHelper = new BaseDeDatosSQLiteHelper(contexto, Constantes.nombreDB, null, 1);

        SQLiteDatabase db = baseDatosHelper.getWritableDatabase();

        String[] args = new String[] {emailCorredor};
        Cursor c = db.rawQuery(Constantes.recuperarCorredorByemail, args);

        if (c != null) {
            if (c.moveToFirst()) {

                int idCursor = c.getInt(c.getColumnIndex(Constantes.idCorredor));
                String nombreCursor = c.getString(c.getColumnIndex(Constantes.nombreCorredor));
                String edadCursor = c.getString(c.getColumnIndex(Constantes.edadCorredor));
                String emailCursor = c.getString(c.getColumnIndex(Constantes.emailCorredor));
                String codigoCursor = c.getString(c.getColumnIndex(Constantes.codigoQR));
                String fechaCursor = c.getString(c.getColumnIndex(Constantes.llegada));

                Corredor corredor = new Corredor(idCursor, nombreCursor, edadCursor, emailCursor, codigoCursor, fechaCursor);

                return corredor;
            }
        }

        c.close();
        db.close();
        return null;
    }

    public static boolean eliminarPregunta(Context contexto, int idRecogido) {

        //Abrimos la base de datos 'DBPreguntas' en modo escritura
        BaseDeDatosSQLiteHelper baseDatosHelper = new BaseDeDatosSQLiteHelper(contexto, Constantes.nombreDB, null, 1);

        SQLiteDatabase db = baseDatosHelper.getWritableDatabase();

        //si la conexion es exitosa
        if (db != null) {

            //Insertamos los datos en la tabla Pregunta
            try {
                String[] args = new String[] {Integer.toString(idRecogido)};
                db.delete(Constantes.tablaCorredores,"id = ?", args);

            } catch (SQLiteException e) {
                return false;
            }
            //Cerramos la base de datos
            db.close();

            return true;

        } else {

            return false;

        }
    }



}
