package com.example.xrun;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

public class CorredorAdapter extends RecyclerView.Adapter<CorredorAdapter.CorredorViewHolder> implements View.OnClickListener {

    private ArrayList<Corredor> items;
    private View.OnClickListener listener;

    /**
     * Clase interna:
     * Se implementa el ViewHolder que se encargará
     * de almacenar la vista del elemento y sus datos
     */
    public static class CorredorViewHolder extends RecyclerView.ViewHolder{


        private TextView TextView_Nombre;
        private TextView TextView_Edad;
        private TextView TextView_Llegada;

        public CorredorViewHolder(View itemView) {
            super(itemView);
            TextView_Nombre = itemView.findViewById(R.id.textViewNombreRow);
            TextView_Edad = itemView.findViewById(R.id.textViewEdadRow);
            TextView_Llegada = itemView.findViewById(R.id.textViewFechaRow);
        }

        public void CorredorBind(Corredor item) {
            TextView_Nombre.setText(item.getNombre());
            TextView_Edad.setText(item.getEdad());
            TextView_Llegada.setText(item.getLlegada());
        }
    }

    // Construye el objeto adaptador recibiendo la lista de datos
    public CorredorAdapter(@NonNull ArrayList<Corredor> items) {
        this.items = items;
    }

    // Se encarga de crear los nuevos objetos ViewHolder necesarios
    // para los elementos de la colección.
    // Infla la vista del layout, crea y devuelve el objeto ViewHolder
    @Override
    public CorredorViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View row = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row, parent, false);
        row.setOnClickListener(this);

        CorredorViewHolder pvh = new CorredorViewHolder(row);
        return pvh;
    }

    // Se encarga de actualizar los datos de un ViewHolder ya existente.
    @Override
    public void onBindViewHolder(CorredorViewHolder viewHolder, int position) {
        Corredor item = items.get(position);
        viewHolder.CorredorBind(item);
    }

    // Indica el número de elementos de la colección de datos.
    @Override
    public int getItemCount() {
        return items.size();
    }

    // Asigna un listener al elemento
    public void setOnClickListener(View.OnClickListener listener) {
        this.listener = listener;
    }

    @Override
    public void onClick(View view) {
        if (listener != null)
            listener.onClick(view);
    }

}
