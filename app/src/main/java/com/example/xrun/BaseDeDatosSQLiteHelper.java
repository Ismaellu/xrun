package com.example.xrun;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class BaseDeDatosSQLiteHelper extends SQLiteOpenHelper {

    public BaseDeDatosSQLiteHelper(Context contexto, String nombre,
    SQLiteDatabase.CursorFactory factory, int version) {
        super(contexto, nombre, factory, version);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        //Se ejecuta la sentencia SQL de creación de la tabla
        db.execSQL(Constantes.crearTabla);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int versionAnterior, int versionNueva) {

        //Se elimina la versión anterior de la tabla
        db.execSQL("DROP TABLE IF EXISTS Corredores");

        //Se crea la nueva versión de la tabla
        db.execSQL(Constantes.crearTabla);
    }
}
