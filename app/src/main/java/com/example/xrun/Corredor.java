package com.example.xrun;

public class Corredor {

    private int id;
    private String nombre;
    private String edad;
    private String email;
    private String codigo;
    private String llegada;

    public Corredor(int id, String nombre, String edad, String email, String codigo) {
        this.id = id;
        this.nombre = nombre;
        this.edad = edad;
        this.email= email;
        this.codigo= codigo;
    }

    public Corredor(int id, String nombre, String edad, String email, String codigo, String llegada) {
        this.id = id;
        this.nombre = nombre;
        this.edad = edad;
        this.email= email;
        this.codigo = codigo;
        this.llegada = llegada;
    }


    public Corredor(String nombre, String edad, String email, String codigo, String llegada) {
        this.nombre = nombre;
        this.edad = edad;
        this.email = email;
        this.codigo = codigo;
        this.llegada = llegada;
    }

    public Corredor(String nombre, String edad, String email, String codigo) {
        this.nombre = nombre;
        this.edad = edad;
        this.email = email;
        this.codigo = codigo;
    }

    public Corredor() {}

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getEdad() {
        return edad;
    }

    public void setEdad(String edad) {
        this.edad = edad;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getEmail() { return email; }

    public void setEmail(String email) { this.email = email; }

    public String getLlegada() {
        return llegada;
    }

    public void setLlegada(String llegada) {
        this.llegada = llegada;
    }
}
