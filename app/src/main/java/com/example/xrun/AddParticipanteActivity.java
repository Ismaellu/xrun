package com.example.xrun;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;


public class AddParticipanteActivity extends AppCompatActivity {

    final private int CODE_WRITE_EXTERNAL_STORAGE_PERMISSION = 123;
    private static final int EMAIL_REQUEST = 230;
    private Context myContext;
    private ConstraintLayout constraintLayoutAddParticipanteActivity;
    private TextView fechaLlegada;
    private EditText nombre;
    private EditText edad;
    private EditText email;
    private String codigo;
    private ImageView QR;
    private Button crear;
    private Button guardar_y_enviar;
    private Button borrar_corredor;
    private String imagePath;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_participante);
        Toolbar toolbar = findViewById(R.id.toolbarAddParticipantes);
        setSupportActionBar(toolbar);

        if (toolbar != null) {
            setSupportActionBar(toolbar);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onBackPressed();
                }
            });
        }
        myContext = this;
        constraintLayoutAddParticipanteActivity = findViewById(R.id.constraintLayoutAddParticipanteActivity);
        fechaLlegada = findViewById(R.id.textViewFechaLlegada);
        nombre = findViewById(R.id.editTextNombre);
        edad = findViewById(R.id.editTextEdad);
        crear =  findViewById(R.id.buttonCrearNuevoCorredor);
        guardar_y_enviar =  findViewById(R.id.buttonGuardarYEnviar);
        borrar_corredor = findViewById(R.id.buttonBorrarCorredor);
        email = findViewById(R.id.editTextEmail);
        QR = findViewById(R.id.imageViewQR);
        final Intent startingIntent = getIntent();

        /**
         * Si la actividad se inicia con un Intent con extras obtenemos ese extra que tiene
         * Que es el ID del corredor y rellenamos la vista con los datos de ese corredor
         */
        if (startingIntent.hasExtra("id")) {

            int idRecuperada = getIntent().getExtras().getInt("id");

            Corredor corredorRecuperado = Repositorio.recuperarCorredor(idRecuperada, myContext);

            fechaLlegada.setText(corredorRecuperado.getLlegada());
            nombre.setText(corredorRecuperado.getNombre());
            edad.setText(corredorRecuperado.getEdad());
            email.setText(corredorRecuperado.getEmail());
            codigo = corredorRecuperado.getCodigo();

            /**
             * Recuperamos la imagen de la BD la convertimos en Bitmap y la pones en el ImageView
             */
            byte [] codigoImagen = Base64.decode(codigo, Base64.DEFAULT);
            Bitmap imagenDecoded = BitmapFactory.decodeByteArray(codigoImagen, 0, codigoImagen.length);

            QR.setImageBitmap(imagenDecoded);
        }

        /**
         * Código para pedir los permisos de escritura de la aplicación en tiempo de ejecución
         */
        int WriteExternalStoragePermission = ContextCompat.checkSelfPermission(myContext, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        Mylog.d("ModificacionPreguntasActivity", "WRITE_EXTERNAL_STORAGE Permission: " + WriteExternalStoragePermission);

        if (WriteExternalStoragePermission != PackageManager.PERMISSION_GRANTED) {
            /**
             * Permiso denegado
             * A partir de Marshmallow (6.0) se pide aceptar o rechazar el permiso en tiempo de ejecución
             * En las versiones anteriores no es posible hacerlo
             */
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
                ActivityCompat.requestPermissions(AddParticipanteActivity.this, new String[] {Manifest.permission.WRITE_EXTERNAL_STORAGE}, CODE_WRITE_EXTERNAL_STORAGE_PERMISSION);
                /**
                 * Una vez que se pide aceptar o rechazar el permiso se ejecuta el método "onRequestPermissionsResult" para manejar la respuesta
                 * Si el usuario marca "No preguntar más" no se volverá a mostrar este diálogo
                 */
            } else {
                Snackbar.make(constraintLayoutAddParticipanteActivity, getResources().getString(R.string.permisoEscrituraGarantizado), Snackbar.LENGTH_LONG).show();
            }
            finish();
        }
        crear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (guardarCorredor(view)) {
                    finish();
                }
            }
        });

        guardar_y_enviar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (guardarCorredor(view)) {
                    guardarYEnviar();
                }
            }
        });

        borrar_corredor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                // Recuperación de la vista del AlertDialog a partir del layout de la Actividad
                LayoutInflater layoutActivity = LayoutInflater.from(myContext);
                final View viewAlertDialog = layoutActivity.inflate(R.layout.alert_dialog_borrar_corredor, null);

                // Definición del AlertDialog
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(myContext);

                // Asignación del AlertDialog a su vista
                alertDialog.setView(viewAlertDialog);

                // Configuración del AlertDialog
                alertDialog.setCancelable(false)
                        // Botón Aceptar
                        .setPositiveButton(getResources().getString(R.string.botonAceptar),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialogBox, int id) {
                                        if (startingIntent.hasExtra("id")) { // comprobamos si venimos de edit
                                            int idRecogido = getIntent().getExtras().getInt("id");

                                            Repositorio.eliminarPregunta(myContext, idRecogido);
                                            Toast.makeText(myContext, "Corredor eliminado correctamente", Toast.LENGTH_SHORT).show();
                                            finish();
                                        } else {
                                            Toast.makeText(myContext, "Este corredor todavía no ha sido creado", Toast.LENGTH_SHORT).show();
                                        }
                                    }
                                })
                        // Botón Cancelar
                        .setNegativeButton(getResources().getString(R.string.botonCancelar),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialogBox, int id) {
                                        dialogBox.cancel();
                                    }
                                })
                        .create()
                        .show();

            }
        });


    }
    private void guardarYEnviar() {
        InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(guardar_y_enviar.getWindowToken(), 0);

        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());
        String emailToQR = email.getText().toString();
        codigo = Metodos.turnBase64(emailToQR);

        byte [] codigoImagen = Base64.decode(codigo, Base64.DEFAULT);
        Bitmap imagenDecoded = BitmapFactory.decodeByteArray(codigoImagen, 0, codigoImagen.length);

        QR.setImageBitmap(imagenDecoded);

        imagePath = Metodos.saveImage(imagenDecoded);

        String[] TO = {email.getText().toString()}; // Correo al que se va a enviar el email
        Intent emailIntent = new Intent(android.content.Intent.ACTION_SEND);
        emailIntent.setType("plain/text");
        emailIntent.putExtra(Intent.EXTRA_EMAIL, TO);
        // Esto podrás modificarlo si quieres, el asunto y el cuerpo del mensaje
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Código QR");
        emailIntent.putExtra(Intent.EXTRA_TEXT, "Este es el código que deberas de enseñar al acabar la carrera");
        emailIntent.putExtra(Intent.EXTRA_STREAM, Uri.parse("file://" + imagePath));
        emailIntent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        try {
            startActivityForResult(Intent.createChooser(emailIntent, "Enviar email..."), EMAIL_REQUEST);
        } catch (android.content.ActivityNotFoundException ex) {
            Toast.makeText(myContext, "No se ha podido enviar el email", Toast.LENGTH_SHORT).show();
        }
    }

    private boolean guardarCorredor(View view) {

        Boolean correcto = false;
        InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(crear.getWindowToken(), 0);

        /**
         * Con estos if y elseif lo que comprobamos es que los campos
         * no se encuentren vacios y en el caso de que intentes crear uno
         * aparezca una snackbar indicandote que los campos estan vacios
         */
        if (nombre.getText().toString().isEmpty()) {
            imm.hideSoftInputFromWindow(crear.getWindowToken(), 0);
            Snackbar.make(view, getResources().getText(R.string.snackBarNombreVacio), Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();
        } else if (edad.getText().toString().isEmpty()) {
            imm.hideSoftInputFromWindow(crear.getWindowToken(), 0);
            Snackbar.make(view, getResources().getText(R.string.snackBarEdadVacio), Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();
        } else if (email.getText().toString().isEmpty()) {
            imm.hideSoftInputFromWindow(crear.getWindowToken(), 0);
            Snackbar.make(view, getResources().getText(R.string.snackBarEmailVacio), Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();
        } else if (QR.getDrawable() == null) {
            imm.hideSoftInputFromWindow(crear.getWindowToken(), 0);
            Snackbar.make(view, getResources().getText(R.string.snackBarQRCode), Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();
        } else if (!Metodos.validarEmail(email.getText().toString())){
            email.setError("Email no válido");
        } else {

            Intent startingIntent = getIntent();

            if (startingIntent.hasExtra("id")) {

                int idRecuperada = getIntent().getExtras().getInt("id");
                Corredor corredorAComprobar = Repositorio.recuperarCorredor(idRecuperada, myContext);
                String emailAntiguo = corredorAComprobar.getEmail();
                String emailNuevo = email.getText().toString();

                if (emailAntiguo.equals(emailNuevo)) {
                    correcto = updateCorredor(idRecuperada, correcto);
                } else {
                    Boolean corredorExists = Metodos.corredorExists(emailNuevo, myContext);
                    if (corredorExists) {
                        Corredor corredorExistente = Repositorio.getCorredorByEmail(emailNuevo, myContext);
                        Toast.makeText(myContext, "El email que ha introducido ya se encuentra registrado, revisar corredor: "+corredorExistente.getNombre(), Toast.LENGTH_SHORT).show();
                    } else {
                        correcto = updateCorredor(idRecuperada, correcto);
                    }
                }
            } else {

                Corredor corredorAComprobar = Repositorio.getCorredorByEmail(email.getText().toString(), myContext);
                if (corredorAComprobar == null) {
                    String emailToQR = email.getText().toString();
                    codigo = Metodos.turnBase64(emailToQR);
                    Corredor c = new Corredor(nombre.getText().toString(), edad.getText().toString(), email.getText().toString(), codigo);
                    if (Repositorio.insertarCorredor(c, myContext)) {
                        Toast.makeText(myContext, "Corredor creado correctamente", Toast.LENGTH_SHORT).show();
                        correcto = true;
                    } else {
                        Toast.makeText(myContext, "No se ha podido crear el corredor", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(myContext, "El email que ha introducido ya existe. Comprobar corredor: "+corredorAComprobar.getNombre(), Toast.LENGTH_SHORT).show();
                }
            }
        }
        return correcto;
    }

    private boolean updateCorredor (int idRecuperada, Boolean correcto) {
        String emailToQR = email.getText().toString();
        codigo = Metodos.turnBase64(emailToQR);
        Corredor corredorRecuperado = new Corredor(nombre.getText().toString(), edad.getText().toString(), email.getText().toString(), codigo);
        if(Repositorio.modificarCorredor(corredorRecuperado, myContext, idRecuperada, null)){
            Toast.makeText(myContext, "Corredor actualizado correctamente", Toast.LENGTH_SHORT).show();
            correcto = true;
        } else {
            Toast.makeText(myContext, "El corredor no se ha podido actualizar", Toast.LENGTH_SHORT).show();
        }
        return correcto;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case EMAIL_REQUEST:
                File file = new File(imagePath);
                file.delete();
//                Toast.makeText(myContext, "El email se ha enviado correctamente", Toast.LENGTH_SHORT).show();
                break;
             default:
                 super.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case CODE_WRITE_EXTERNAL_STORAGE_PERMISSION:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    //Metodos a ejecutar si se han garantizado los permisos
                    Snackbar.make(constraintLayoutAddParticipanteActivity, getResources().getString(R.string.internet_permission_granted), Snackbar.LENGTH_LONG)
                            .show();
                } else {
                    Snackbar.make(constraintLayoutAddParticipanteActivity, getResources().getString(R.string.internet_permission_denied), Snackbar.LENGTH_LONG)
                            .show();
                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    @Override
    public boolean onNavigateUp() {
        // Vuelve a la pantalla de resumen
        return true;
    }

    @Override
    public void onBackPressed() {
        finish();
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

    }

    @Override
    protected void onRestart() {
        super.onRestart();
    }

}
