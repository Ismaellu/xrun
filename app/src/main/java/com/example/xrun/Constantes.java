package com.example.xrun;

public class Constantes {

    public static final float VIEWPORT_WIDTH = 5.0f;
    public static final String nombreDB = "DBCorredores";
    public static final String tablaCorredores = "Corredores";
    public static final String idCorredor = "id";
    public static final String nombreCorredor = "nombre";
    public static final String edadCorredor = "edad";
    public static final String emailCorredor = "email";
    public static final String codigoQR = "codigo";
    public static final String llegada = "llegada";
    public static final String crearTabla = "CREATE TABLE " + tablaCorredores + " ("+idCorredor+" INTEGER PRIMARY KEY AUTOINCREMENT, " +nombreCorredor+ " TEXT, "+edadCorredor+" TEXT, "+emailCorredor+" TEXT, "+codigoQR+" TEXT, "+llegada+" TEXT)";
    public static final String consultarTabla = " SELECT * FROM " + tablaCorredores + " ORDER BY "+nombreCorredor;
    public static final String recuperarCorredorModificar = "SELECT * FROM " + tablaCorredores + " WHERE " + idCorredor + " = ?";
    public static final String recuperarCorredorByemail = "SELECT * FROM " + tablaCorredores + " WHERE " + emailCorredor + " = ?";
}
