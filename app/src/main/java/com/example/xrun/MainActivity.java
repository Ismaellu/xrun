package com.example.xrun;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

public class MainActivity extends AppCompatActivity  {

    private Context myContext;
    private IntentIntegrator qrScan;
    private ImageView clickEvent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        ImageView clickEvent = findViewById(R.id.imageViewScanQRCode);
        setSupportActionBar(toolbar);
        myContext = this;
        qrScan = new IntentIntegrator(this);

        clickEvent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(myContext, "Scan QR Code", Toast.LENGTH_SHORT).show();

                qrScan.setDesiredBarcodeFormats(IntentIntegrator.QR_CODE_TYPES);
                qrScan.setPrompt("Scan");
                qrScan.setCameraId(0);
                qrScan.setBeepEnabled(false);
                qrScan.setBarcodeImageEnabled(false);
                qrScan.setOrientationLocked(false);
                qrScan.initiateScan();
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
        IntentResult scanResult = IntentIntegrator.parseActivityResult(requestCode, resultCode, intent);
        if (scanResult != null) { // Cambiar if
            // handle scan result
            String emailRecuperado = intent.getStringExtra("SCAN_RESULT");
            Corredor corredorRecuperado = Repositorio.getCorredorByEmail(emailRecuperado, myContext);
            if (corredorRecuperado != null) {
                Integer idCorredor = corredorRecuperado.getId();
                String llegadaRecuperada = corredorRecuperado.getLlegada();

                if (llegadaRecuperada.equals("El corredor aun no ha llegado")) {
                    String fechaLlegada = Metodos.getDateCarrera();
                    Repositorio.modificarCorredor(corredorRecuperado, myContext, idCorredor, fechaLlegada);
                    Toast.makeText(myContext, "Se ha actualizado la hora de llegada de "+corredorRecuperado.getNombre()+ " con fecha: "+fechaLlegada, Toast.LENGTH_LONG).show();
                } else {
                    Toast.makeText(myContext, "Este corredor ya ha llegado a la linea de meta. Revisar: "+corredorRecuperado.getNombre(), Toast.LENGTH_LONG).show();
                }
            } else {
                Toast.makeText(myContext, "Este corredor no se encuentra listado", Toast.LENGTH_LONG).show();
            }
        } else {
            super.onActivityResult(requestCode, resultCode, intent);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    // Codigo que permite el funcionamiento de la ActionBar de la MainActivity
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.action_acerca_de:
                startActivity(new Intent(MainActivity.this, AcercaDeActivity.class));
                return true;
            case R.id.action_listadoPreguntas:
                startActivity(new Intent(MainActivity.this, ListadoParticipantesActivity.class));
                return true;
            case R.id.action_salir:
                finish();
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

    }

    @Override
    protected void onRestart() {
        super.onRestart();
    }

}
