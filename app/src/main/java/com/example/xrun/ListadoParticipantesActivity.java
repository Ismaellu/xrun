package com.example.xrun;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;

import java.util.ArrayList;

public class ListadoParticipantesActivity extends AppCompatActivity {

    private ArrayList<Corredor> itemsCorredor;
    private Context myContext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        myContext = this;

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listado_participantes);
        Toolbar toolbar = findViewById(R.id.toolbarListadoParticipantes);
        setSupportActionBar(toolbar);

        if (toolbar != null) {
            setSupportActionBar(toolbar);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setTitle(getResources().getString(R.string.title_activity_listado_participantes));
            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    // Volver atras en las pantallas
                    onBackPressed();
                }
            });
        } else {
            Log.d("SobreNosotros", "Error al cargar toolbar");
        }

        FloatingActionButton fab = findViewById(R.id.buttomAddParticipante);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(ListadoParticipantesActivity.this, AddParticipanteActivity.class));
            }
        });
    }

    //Este metodo sobreescribe el boton  de nuestro telefono de back
    @Override
    public void onBackPressed() {
        //Vuelve a la pantalla de inicio haciendole un finish a la actividad
        finish();
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (Repositorio.consultarBD(myContext) != null) {
            itemsCorredor = Repositorio.consultarBD(myContext);
            final RecyclerView recyclerViewListado = findViewById(R.id.recyclerViewListado);

            //Creacion del adaptador con los datos de la lista anterior
            CorredorAdapter adaptador = new CorredorAdapter(itemsCorredor);

            //Accion al pulsar sobre un elemento de la lista
            adaptador.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                int position = recyclerViewListado.getChildAdapterPosition(v);

                Intent it = new Intent(ListadoParticipantesActivity.this, AddParticipanteActivity.class);
                it.putExtra("id", itemsCorredor.get(position).getId());
                startActivity(it);

                }
            });

            // Asocia el Adaptador al RecyclerView
            recyclerViewListado.setAdapter(adaptador);

            // Muestra el RecyclerView en vertical
            recyclerViewListado.setLayoutManager(new LinearLayoutManager(myContext));
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() { super.onDestroy(); }

    @Override
    protected void onRestart() {
        super.onRestart();
    }

}
