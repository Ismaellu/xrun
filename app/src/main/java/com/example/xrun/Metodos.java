package com.example.xrun;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Environment;
import android.util.Base64;
import android.util.Patterns;
import net.glxn.qrgen.android.QRCode;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Random;
import java.util.regex.Pattern;

public class Metodos {

    public static boolean emailExists(String email, Context myContext) {

        Corredor corredorRecuperado = Repositorio.getCorredorByEmail(email, myContext);

        if (corredorRecuperado == null) {
            return false;
        } else {
            return true;
        }
    }

    public static boolean corredorExists(String email, Context mycontext) {

        Corredor corredorExists = Repositorio.getCorredorByEmail(email, mycontext);

        if (corredorExists != null) {
            return true;
        } else {
            return false;
        }
    }

    public static boolean validarEmail(String email) {
        Pattern pattern = Patterns.EMAIL_ADDRESS;
        return pattern.matcher(email).matches();
    }

    public static String getDateCarrera() {

        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm");
        String date = dateFormat.format(calendar.getTime());

        return date;
    }

    public static String turnBase64(String stringToQR){

        if(stringToQR != null) {

            ByteArrayOutputStream stream = QRCode.from(stringToQR).stream();
            byte[] byteArray = stream.toByteArray();
            String imagen64 = Base64.encodeToString(byteArray, 0);
            return imagen64;
        } else {
            String imagen64 = "";
            return imagen64;
        }

    }

    public static String saveImage(Bitmap finalBitmap) {

        String root = Environment.getExternalStorageDirectory().toString();
        File myDir = new File(root + "/saved_images");
        myDir.mkdirs();

        Random generator = new Random();
        int n = 10000;
        n = generator.nextInt(n);
        String fname = "QRCode"+ n +".jpg";

        File file = new File(myDir, fname);
        String imagepath = root + "/saved_images/" + fname;
        if (file.exists()) file.delete ();
        try {
            FileOutputStream out = new FileOutputStream(file);
            finalBitmap.compress(Bitmap.CompressFormat.JPEG, 90, out);
            out.flush();
            out.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return imagepath;
    }
}
