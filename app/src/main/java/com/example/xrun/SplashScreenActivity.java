package com.example.xrun;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class SplashScreenActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
        //Aqui empieza el contador de segundos para el splashscreen
        int secondsDelayed = 5;
        new Handler().postDelayed(new Runnable() {
            public void run() {
                //En el primer activity hay que poner el nombre de la actividad en la que esta y en el segundo la actividad
                //a la que quieres que te lleve
                startActivity(new Intent(SplashScreenActivity.this, MainActivity.class));
                finish();
            }
            //Aqui se ponen los milisegundos que tarda
        }, secondsDelayed * 1000);
    }
}
